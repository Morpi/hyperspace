	// Add .active for link in header menu
	// target.position().top - Дает позицию всех якорей которые есть на странице
	// в нутри идентификатора #sidebar
	// scroll_top - Дает текущую позицию, на сколько прокручена страница,
	// относительно верха
	$(window).on('scroll load', function() {
		var scroll_top = $(document).scrollTop();
		$("#sidebar a").each(function() {
			var hash = $(this).attr("href"),
			target = $(hash),
			height = $(window).height();
			if (target.position().top <= (scroll_top+height/2.5)) {
				$("#sidebar a.active").removeClass("active");
				$(this).addClass("active");
			} else {
				$(this).removeClass("active");
			}
		});
	});

	/* 
	Проверяем загружен ли документ и был ли ресайз окна,
	если документ загружен выполняеться условие, а если документ был ресайзнут,
	тоже выполняем условие но с постоянной проверкой

	Сама функция делает прокрутку к якорю с анимаций и задержкой в 1500мс

	Если условие верно, то документ будет прокручен к якорю с отступом
	с верху документа на высоту меню, которое зафиксировано	
	*/
	$(window).on('load resize', function() {
		var $width = $(window).width();
		if($width < 1199) {
			$('a').on('click', function() {
				var hash = $(this).attr('href'),
				target = $(hash),
				$rem = $('#sidebar').css('height'),
				$px = $rem.substring(0,2);

				$('html, body').stop().animate({
					'scrollTop': target.offset().top - $px
				}, 1500);
			});
		} else {
			$('a').on('click', function() {
				var hash = $(this).attr('href'),
				target = $(hash);

				$('html, body').stop().animate({
					'scrollTop': target.offset().top
				}, 1500);
			});
		}
	})

	$(document).ready(function() {	
	// Initialization WOW.js
	var $px = ($(document).height()/50);
	wow = new WOW({
		offset: $px,
	})
	wow.init();

	// animation delay for fixed nav-menu
	$('#sidebar li').each(function() {
		var $delay = $(this).attr('data-delay')
		$(this).css({'animation-delay' : $delay});
	});

});